# CppPkg

#### 介绍
C++的包管理windows软件

#### 软件架构
底层使用vcpkg,人机界面使用C#/WPF


#### 安装教程
下载并安装最新版的下列软件
1.  git x64    (例：https://dl.softmgr.qq.com/original/Development/Git-2.33.1-64-bit.exe)
2.  PowerShell win-x86    (例：https://github.com/PowerShell/PowerShell/releases/download/v7.1.5/PowerShell-7.1.5-win-x86.msi)
3.  CMake x64    （例：https://github.com/Kitware/CMake/releases/download/v3.22.0-rc2/cmake-3.22.0-rc2-windows-x86_64.msi）
安装完毕后重启电脑，下载发行包并解压缩到目标文件夹内

### 标题

#### 使用说明

1.  初次使用时先点击 初始化环境变按钮
2.  在根CMakeList.txt文件中,project前加入行
    set(CMAKE_TOOLCHAIN_FILE $ENV{CppPkg})
3.  部分库在国内源无法使用，取消勾选使用国内源->刷新所有库，再尝试下载
4.  第一次下载库会比较慢，因为会下载相应的环境，建议使用国内源进行第一次下载

#### 参与贡献

1.  Fork 本仓
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
