﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CppPkg
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SortedDictionary<string, string> _allSortedLibs = new();// 有序所有的库
        Dictionary<string, string> _allHashLibs = new();// 无序所有的库
        HashSet<string> _allLibKeys = new();// 所有的库名
        public MainWindow()
        {
            InitEnv.AllocConsole();

            // 自带的UI组件初始化
            InitializeComponent();
            // 自定义的初始化
            initialize();
        }
        ~MainWindow() => InitEnv.FreeConsole();

        // 初始化
        void initialize()
        {
            initConnect();
            initAllLibs();
            initCurLibs();
            initEnv();
        }

        // 初始化所有库
        void initAllLibs()
        {
            // 手动触发全部库刷新事件
            btnAllLibsRefresh.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
        }

        // 初始化当前已有的库
        void initCurLibs()
        {
            // 手动触发当前库刷新事件
            btnCurLibsRefresh.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
        }

        // 当前库加入新项
        void OnCurLibNameAdd(string arg)
        {
            // 创建异步调用
            listCurLibs.Items.Dispatcher.Invoke(new Action(() =>
            {
                // 有效项则加入
                if (arg?.Length > 0) listCurLibs.Items.Add(arg);
            }));
        }

        // 所有库加入新项
        void OnAllLibNameAdd(string arg)
        {
            // 创建异步调用
            listAllLibs.Items.Dispatcher.Invoke(new Action(() =>
            {
                // 有效项则加入
                if (arg?.Length > 0)
                {
                    // 加入显示控件
                    listAllLibs.Items.Add(arg);
                    // 存入键值对
                    var tempName = arg.Substring(0, arg.IndexOf(' '));
                    _allSortedLibs.Add(tempName, arg);
                    _allHashLibs.Add(tempName, arg);
                    _allLibKeys.Add(tempName);
                }
            }));
        }

        // 初始化注册
        void initConnect()
        {
            // 所有库更新时,单项异步加入
            VcPkgCtl.SplitAllItem += OnAllLibNameAdd;
            // 当前已有库刷新时,单项加入
            VcPkgCtl.CurLibsName += OnCurLibNameAdd;
            // 增加当前库
            VcPkgCtl.CurLibsAdd += () =>
            {// 触发当前库刷新按钮
                btnCurLibsRefresh.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            };
        }

        // 初始化环境变量
        void initEnv()
        {
            // 换源为国内
            Environment.SetEnvironmentVariable("X_VCPKG_ASSET_SOURCES", @"x-azurl,http://106.15.181.5/");
            // 编译为x64-windows的库
            Environment.SetEnvironmentVariable("VCPKG_DEFAULT_TRIPLET", @"x64-windows");
        }

        // 刷新当前已有库
        private void btnCurLibsRefresh_Click(object sender, RoutedEventArgs e)
        {
            // 删除所有项
            listCurLibs.Items.Clear();
            // 查找已有项
            VcPkgCtl.CurLibs();
        }

        // 删除库
        private void btnDeleteLib_Click(object sender, RoutedEventArgs e)
        {
            var item = listCurLibs.SelectedItem as string;
            if (item?.Length > 0)
            {// 有效项
                VcPkgCtl.DeleteLib(item.Substring(0, item.IndexOf(' ')));
            }
        }

        // 刷新所有库
        private void btnAllLibsRefresh_Click(object sender, RoutedEventArgs e)
        {
            // 删除所有项
            listAllLibs.Items.Clear();
            _allSortedLibs.Clear();
            _allHashLibs.Clear();
            _allLibKeys.Clear();
            // 查找已有项
            VcPkgCtl.AllLibs();
        }

        // 添加库
        private void btnAddLib_Click(object sender, RoutedEventArgs e)
        {
            var itemName = listAllLibs.SelectedItem as string;
            if (itemName?.Length > 0)
            {
                var libName = itemName.Substring(0, itemName.IndexOf(' '));
                VcPkgCtl.AddLib(libName);
            }
        }

        private void OnSearchLib(object sender, TextChangedEventArgs e)
        {
            listAllLibs.Items.Clear();// 先清除

            // 获得输入的库名
            var subLibName = textSearchLib.Text;

            // 当前未输入参数,显示所有库并返回
            if (subLibName.Length == 0)
            {
                var tempVals = _allSortedLibs.Values;// 获得所有的值
                // 显示
                foreach (var item in tempVals)
                {
                    listAllLibs.Items.Add(item);
                }
                return;
            }

            // 显示搜索的库
            List<string> tempLibNames = new();
            // 查找
            foreach (var item in _allLibKeys)
            {
                if (item.Contains(subLibName))
                {
                    tempLibNames.Add(item);
                }
            }
            // 清除并显示
            foreach (var item in tempLibNames)
            {
                string libName = null;
                if (_allHashLibs.TryGetValue(item, out libName))
                {
                    listAllLibs.Items.Add(libName);
                }
            }
        }

        // 加入用户环境变量,用来使用CMake
        private void btnInitEnv_Click(object sender, RoutedEventArgs e)
        {
            // exe所在目录
            var rootDir = System.Environment.CurrentDirectory;
            // CMake所需目录
            var needDir = rootDir + @"\data\scripts\buildsystems\vcpkg.cmake";
            // 写入
            Environment.SetEnvironmentVariable("CppPkg", needDir, EnvironmentVariableTarget.User);
            // 显示提示信息
            MessageBox.Show("在顶层CMakeLists.txt project之前加入行:\n" + @"set(CMAKE_TOOLCHAIN_FILE $ENV{CppPkg})");
        }

        // 移除环境变量
        private void btnRemoveEnv_Click(object sender, RoutedEventArgs e)
        {
            // 删除
            Environment.SetEnvironmentVariable("CppPkg", null, EnvironmentVariableTarget.User);
            // 显示提示信息
            MessageBox.Show("已删除CppPkg环境变量");
        }

        private void CheckChanged(object sender, RoutedEventArgs e)
        {
            var isChecked = cBoxIsUseMirror.IsChecked;
            if (isChecked == null) return;
            if (isChecked == true)
            {// 使用镜像
             // 换源为国内
                Environment.SetEnvironmentVariable("X_VCPKG_ASSET_SOURCES", @"x-azurl,http://106.15.181.5/");
            }
            else
            {
             // 换源为国外
                Environment.SetEnvironmentVariable("X_VCPKG_ASSET_SOURCES", null);
            }
        }

        private void TextPkgPath_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            // 记录当前的路径
            VcPkgCtl.VcpkgPath=textPkgPath.Text;
        }
    }
}
