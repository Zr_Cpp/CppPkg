﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CppPkg
{
    class InitEnv
    {
        /// <summary>
        /// Allocates a new console for current process.
        /// </summary>
        [DllImport("kernel32.dll")]
        public static extern Boolean AllocConsole();

        /// <summary>
        /// Frees the console.
        /// </summary>
        [DllImport("kernel32.dll")]

        public static extern Boolean FreeConsole();

        // 检查git是否存在
        public static void checkGitExist()
        {
            bool isExist = false;
            Microsoft.Win32.RegistryKey uninstallNode = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall");
            foreach (string subKeyName in uninstallNode.GetSubKeyNames())
            {
                Microsoft.Win32.RegistryKey subKey = uninstallNode.OpenSubKey(subKeyName);
                object displayName = subKey.GetValue("DisplayName");
                if (displayName is string exeName)
                { 
                    if(exeName.Contains("Git version"))
                    {
                        isExist = true;
                    }
                }
            }
            if (!isExist)
            {// 未找到git,退出程序
                MessageBox.Show("please install Git and retry");
                Application.Current.Shutdown();
            }
        }
        // 从git上下载vcpkg
        public static void downloadData()
        {
            Console.WriteLine("initialize start,please wait...");
            var info = new ProcessStartInfo("git", @"clone https://gitee.com/mirrors/vcpkg.git ./data")
            {
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                WorkingDirectory = System.IO.Path.GetDirectoryName("git"),
            };
            var process = new Process
            {
                StartInfo = info,
            };
            process.Start();
            Console.WriteLine(process.StandardOutput.ReadToEnd());
            // 输出目录
            process.OutputDataReceived += (obj, data) =>
            {
                Console.WriteLine(data.Data);
            };
            process.ErrorDataReceived += (obj, data) =>
            {
                Console.WriteLine(data.Data);
            };
            process.WaitForExit();
            Console.WriteLine("initialize finish");
        }
    }
}
