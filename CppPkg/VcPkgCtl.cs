﻿using System;
using System.Diagnostics;
using System.IO;

namespace CppPkg;

internal class VcPkgCtl
{
    // 增加当前库后刷新
    public delegate void CurLibsAddHandle();

    /*********当前库**********/
    // 当前库刷新
    public delegate void CurLibsNameHandle(string arg);

    // 所有库
    public delegate void SplitAllItemHandle(string arg);

    private const string Order_Install = "install";
    private const string Order_Remove = "remove";

    private static bool _isSearchAllLibInvalid = true; // 是否剩下项有效
    private static bool _isSearchAllLibBusy; // 是否正在繁忙获得All项

    // vcpkg的路径
    public static string VcpkgPath { get; set; }

    public static event SplitAllItemHandle SplitAllItem;
    public static event CurLibsNameHandle CurLibsName;
    public static event CurLibsAddHandle CurLibsAdd;

    private static async void commandLine(string arg)
    {
        // 检查是否有vcpkg.exe
        if (!File.Exists(VcpkgPath + '/' + "vcpkg.exe")) return;
        var info = new ProcessStartInfo("./data/vcpkg", arg)
        {
            CreateNoWindow = true,
            RedirectStandardOutput = true,
            UseShellExecute = false
            //WorkingDirectory = "./data/vcpkg",
        };
        var process = new Process
        {
            StartInfo = info
        };
        process.Start(); // 启动
        process.OutputDataReceived += (_, e) => { Console.WriteLine(e.Data); }; // 广播
        process.BeginOutputReadLine(); // 开始读取
        await process.WaitForExitAsync(); // 等待读取完毕
        process.Dispose(); // 释放
        // 查看当前的操作指令
        switch (arg.Substring(0, arg.IndexOf(' ')))
        {
            case Order_Install: // 增加后刷新当前库
                CurLibsAdd?.Invoke();
                break;
            case Order_Remove: // 删除后也刷新下当前库
                CurLibsAdd?.Invoke();
                break;
        }
    }

    /// <summary>
    /// 同步cmd指令
    /// </summary>
    /// <param name="cmd">cmd的指令</param>
    /// <param name="func">接收到cmd返回消息后的处理函数</param>
    public static async void AsyncCmd(string cmd, DataReceivedEventHandler func)
    {
        // 检查是否有vcpkg.exe
        var tempPkgPath = VcpkgPath + '/' + "vcpkg.exe";
        if (!File.Exists(tempPkgPath)) return;

        var info = new ProcessStartInfo(tempPkgPath, cmd)
        {
            CreateNoWindow = true,
            RedirectStandardOutput = true,
            UseShellExecute = false
            //WorkingDirectory = "./data/vcpkg",
        };
        var process = new Process
        {
            StartInfo = info
        };
        process.Start(); // 启动
        process.OutputDataReceived += func; // 广播
        process.BeginOutputReadLine(); // 开始读取
        await process.WaitForExitAsync(); // 等待读取完毕
        process.Dispose(); // 释放
    }

    // 查找所有库的时候,发送单个库
    private static void SingleLibBroad(object sender, DataReceivedEventArgs e)
    {
        Console.WriteLine(e.Data);
        // 在有效时查看是否剩余项有效
        if (_isSearchAllLibInvalid) _isSearchAllLibInvalid = !e.Data.Contains("git pull");
        // 更新所有库
        if (_isSearchAllLibInvalid)
            // 发送
            SplitAllItem?.Invoke(e.Data);
    }

    // 查找所有库
    public static void AllLibs()
    {
        _isSearchAllLibInvalid = true; // 初始都有效
        if (_isSearchAllLibBusy) return; // 忙碌时返回
        _isSearchAllLibBusy = true; // 繁忙
        AsyncCmd("search", SingleLibBroad);
        _isSearchAllLibBusy = false;
    }

    // 安装指定库
    public static void AddLib(string libName)
    {
        commandLine($"{Order_Install} {libName}");
    }

    // 删除指定库
    public static void DeleteLib(string libName)
    {
        commandLine($"{Order_Remove} {libName}");
    }

    // 返回当前已有的库
    public static void CurLibs()
    {
        AsyncCmd("list", (_, e) =>
        {
            if (e == null || e.Data == null) return;
            // 在有效时查看是否剩余项有效
            var curMsg = e.Data;
            var isEmpty = e.Data.Contains(@"No packages are installed. Did you mean `search`?");
            // 更新所有库
            if (!isEmpty)
                // 发送
                CurLibsName?.Invoke(e.Data);
        });
    }
}